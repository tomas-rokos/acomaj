#!/bin/bash

pip install pytest radon pylint
pip install -r ./functions/headliner/requirements.txt
pip install -r ./functions/ingester/requirements.txt
pip install -r ./scripts/requirements.txt
