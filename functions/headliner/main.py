from google.cloud import firestore

from menu_headliner import Headliner


def headliner(data, context):
    parts = context.resource.split('/')

    menu = parts[6]

    menu_doc = firestore.Client().collection('menu').document(menu)
    menu_data = menu_doc.get()
    items = menu_data.get('items')

    headliner = Headliner()

    for item in items:
        if item.get('short') is not None:
            continue
        orig: str = item.get('orig')
        if 'TÝDNE:' in orig or 'Týdenní' in orig:
            continue
        item['short'] = headliner.predict(orig)

    menu_doc.update({'items': items})

