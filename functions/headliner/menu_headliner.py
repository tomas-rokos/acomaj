import os
import json
import sys

import pickle


class Headliner:
    def __init__(self):
        with open(os.path.join(sys.path[0], 'vectorizer'), 'rb') as f:
            self.vectorizer = pickle.load(f)
        with open(os.path.join(sys.path[0],'meals.json')) as f:
            self.meals = json.load(f)
        with open(os.path.join(sys.path[0],'model'), 'rb') as f:
            self.model = pickle.load(f)

    def predict(self, name: str) -> str:
        tick = self.vectorizer.transform([name])
        predictions = self.model.predict(tick)
        for prediction in predictions:
            return self.meals[prediction]
        return ''
