from google.cloud import firestore

from providers import beranek, presto, khomfi, salanda, zomato


def ingester(data, context):
    parts = context.resource.split('/')

    place = parts[6]
    menu_date = parts[8]

    place_doc = firestore.Client().collection('places').document(place)
    rq_doc =  place_doc.collection('ingest').document(menu_date)

    if place == 'presto':
        print('Will ingest Presto')
        meals, req_str = presto.ingest()
    elif place == 'khomfi':
        print('Will ingest Khomfi')
        meals, req_str = khomfi.ingest()
    elif place == 'salanda':
        print('Will ingest Šalanda')
        meals, req_str = salanda.ingest()
    elif place == 'modry-beranek':
        print('Will ingest Beránek')
        meals, req_str = beranek.ingest()
    else:
        place_data = place_doc.get()
        meals, req_str = zomato.ingest(place_data.get('zomato_id'))
    rq_doc.set({
        'html': req_str
    })
    new_menu = {
        'date': menu_date,
        'place': place,
        'items': meals
    }
    print(new_menu)
    firestore.Client().collection('menu').document(menu_date + '-' + place) \
        .set(new_menu)

