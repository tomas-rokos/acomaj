from typing import List

import requests
from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self.meals = []
        self.curr_path = []
        self.ingest_level = 0
        self.curr_ingest = ''

    @staticmethod
    def _get_attr(attrs, name: str) -> str:
        for attr in attrs:
            if attr[0] == name:
                return attr[1]
        return None

    def handle_starttag(self, tag, attrs):
        self.curr_path.append(tag)
        if self._get_attr(attrs, 'id') == 'daily-meals':
            self.ingest_level = len(self.curr_path)

    def handle_endtag(self, tag):
        while True:
            poped = self.curr_path.pop()
            if poped == tag:
                break
        ingest = self.curr_ingest.strip(' \t\n\r' + chr(160))
        ingest = ingest.replace(chr(160), ' ')
        if len(ingest):
            if 'td' in self.curr_path and 'K polednímu menu' not in ingest:
                self.meals.append(ingest)
            self.curr_ingest = ''
        if len(self.curr_path) and tag == 'table':
            self.ingest_level = 0
        if len(self.curr_path) < self.ingest_level:
            self.ingest_level = 0

    def handle_data(self, data):
        if self.ingest_level != 0:
            self.curr_ingest += data


def ingest() -> [List[str], str]:
    resp = requests.get('https://www.restauracesalanda.cz/cs/salanda/karlin/')
    text = resp.text
    print(f'Downloaded {len(text)} bytes')
    parser = MyHTMLParser()
    parser.feed(resp.text)
    print(f'Ingested {len(parser.meals)} meals')
    return [{'orig': s} for s in parser.meals], resp.text

