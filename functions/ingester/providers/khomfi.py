from typing import List

import requests
from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.curr_path = []
        self.ingest_level = 0
        self.meals = []
        self.act_meal = None

    def handle_starttag(self, tag, attrs):
        self.curr_path.append(tag)
        # if self.ingest_level != 0:
        #     print(self.curr_path[slice(self.ingest_level, -1)])

    def handle_endtag(self, tag):
        while True:
            poped = self.curr_path.pop()
            if poped == tag:
                break
        if self.ingest_level != 0:
            depth = len(self.curr_path) - self.ingest_level
            if depth < 3 and self.act_meal is not None:
                if len(self.meals) == 0 or self.act_meal.get('short') == 'Polévka':
                    self.act_meal.pop('short')
                self.meals.append(self.act_meal)
                self.act_meal = None
            # print(self.curr_path[slice(self.ingest_level, -1)])
        if len(self.curr_path) < self.ingest_level:
            self.ingest_level = 0

    def handle_data(self, data):
        # print(self.curr_path[-1:], data)
        if self.curr_path[-1:] == ['h1'] and 'Polední nabídka' in data:
            self.ingest_level = len(self.curr_path) - 3
        else:
            if self.ingest_level != 0:
                ingest = data.strip(' \t\n\r' + chr(160))
                if ingest != '':
                    if self.act_meal is None:
                        self.act_meal = {
                            'short': ingest,
                            'orig':  ingest
                        }
                    else:
                        self.act_meal['orig'] += ' ' + ingest
                    # print(self.curr_path[slice(self.ingest_level, -1)], data)


def ingest() -> [List[str], str]:
    resp = requests.get('https://www.khomfi.cz/menu/poledni-nabidka/')
    text = resp.text
    print(f'Downloaded {len(text)} bytes')
    parser = MyHTMLParser()
    parser.feed(resp.text)
    print(f'Ingested {len(parser.meals)} meals')
    return parser.meals, resp.text
