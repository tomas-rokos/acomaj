from typing import List

import requests
from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self.meals = []
        self.curr_path = []
        self.ingest_level = 0
        self.curr_ingest = ''

    def handle_starttag(self, tag, attrs):
        self.curr_path.append(tag)

    def handle_endtag(self, tag):
        while True:
            poped = self.curr_path.pop()
            if poped == tag:
                break
        ingest = self.curr_ingest.strip(' \t\n\r' + chr(160))
        if tag == 'p' and len(ingest):
            if ingest.startswith('Menu na '):
                pass
            elif ingest == 'Menu:':
                pass
            else:
                self.meals.append(ingest)
            self.curr_ingest = ''
        if len(self.curr_path) < self.ingest_level:
            self.ingest_level = 0

    def handle_data(self, data):
        if data == 'Nabídka:':
            self.ingest_level = len(self.curr_path) - 1
        else:
            if self.ingest_level != 0:
                self.curr_ingest += data


def ingest() -> [List[str], str]:
    resp = requests.get('http://www.meat-market.cz/bistro')
    text = resp.text
    print(f'Downloaded {len(text)} bytes')
    parser = MyHTMLParser()
    parser.feed(resp.text)
    print(f'Ingested {len(parser.meals)} meals')
    return [{'orig': s} for s in parser.meals], resp.text
