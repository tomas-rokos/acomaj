import os
from typing import List

import requests


def ingest(res_id: str) -> [List[str], str]:
    url = f'https://developers.zomato.com/api/v2.1/dailymenu?res_id={res_id}'
    headers = {
        'user_key': os.environ['ZOMATO_KEY']
    }
    result = requests.get(url, headers=headers)
    meals = [{'orig': dish['dish']['name']} for dish in result.json()['daily_menus'][0]['daily_menu']['dishes']]
    return meals, result.text
