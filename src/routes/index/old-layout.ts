import * as _ from 'lodash';

import { transformRaw, calcRect, RIGHT, LEFT } from './layout';

export class Layout {
    basicMultiplier = 0;
    cards = [];
    _width: number;

    set width(val: number) {
        this._width = val;
        this.basicMultiplier = val / (RIGHT - LEFT);
    }

    transform(pnt): [number, number] {
        return transformRaw(pnt, this.basicMultiplier);
    }

    calcNewCard(geoPos, charsInRow: number, rows: number): {left: number, top: number} {
        const grPos = this.transform([geoPos.longitude, geoPos.latitude]);
        const width = charsInRow * 5.5 + 4;
        const height = rows * 16.5 + 4;
        const card = {x1: grPos[0] - width / 2, x2: grPos[0] + width / 2, y1: grPos[1] - height / 2, y2: grPos[1] + height / 2};
        _.forEach(this.cards, (otherCard) => {
            if (card.y1 > otherCard.y2 || card.y2 < otherCard.y1) {
                return;
            }
            if (card.x1 > otherCard.x2 || card.x2 < otherCard.x1) {
                return;
            }
            if (card.y1 > otherCard.y1) {
                const delta = otherCard.y2 + 10 - card.y1;
                card.y1 += delta;
                card.y2 += delta;
            } else {
                const delta = otherCard.y1 - 10 - card.y2;
                card.y1 += delta;
                card.y2 += delta;
            }
        });
        this.cards.push(card);
        return {left: (card.x1 + card.x2) / 2, top: 40 + (card.y1 + card.y2) / 2};
    }
    alignCards(cards) {
        cards.forEach((card) => {
            const width = card.content.reduce((total, contentItem) => Math.max(total, contentItem.length), 0);
            const height = card.content.length;
            card['screenPos'] = this.calcNewCard(card['geo'], width, height);
        });
    }
}
