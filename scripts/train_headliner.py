import json

from numpy import arange
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import classification_report
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

import pickle


with open('./data/menus.json') as f:
    menus = json.load(f)

items = []
result_strings = []
for menu in menus:
    for item in menu['items']:
        if isinstance(item, dict) and item.get('short'):
            items.append(item['orig'])
            result_strings.append(item['short'])

with open('./data/stop_words.json') as f:
    stop_words = json.load(f)

vectorizer = CountVectorizer(stop_words=stop_words)
vectors = vectorizer.fit_transform(items)

s = pickle.dumps(vectorizer)
with open('./functions/headliner/vectorizer', 'wb') as f:
    f.write(s)
print(json.dumps(vectorizer.get_feature_names(), ensure_ascii=False, indent=True))

meals = list(dict.fromkeys(result_strings))
with open('./functions/headliner/meals.json', 'w') as f:
    json.dump(meals, f)
result_keys = [meals.index(result) for result in result_strings]

# model = SVC(gamma='auto')
# model = KNeighborsClassifier()
model = DecisionTreeClassifier()
# model = GaussianNB()

model.fit(vectors, result_keys)

predictions = model.predict(vectors)
print(classification_report(result_keys, predictions))

s = pickle.dumps(model)
with open('./functions/headliner/model', 'wb') as f:
    f.write(s)
