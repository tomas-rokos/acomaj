import json
from xml.etree import ElementTree


root = ElementTree.parse('./data/map.xml').getroot()

nodes = {}


def dumpWay(item, type, name):
    addit = '{"highway": "' + type + '", "name": "' + name + '"}'
    print('{"type": "Feature","properties": ' + addit + ',"geometry": {"type": "LineString","coordinates": [')
    first = True
    for subitem in item:
        if subitem.tag == 'nd':
            if first:
                first = False
            else:
                print(',')
            node = nodes[subitem.attrib.get('ref')]
            print('[')
            print(node.attrib['lon'], ',')
            print(node.attrib['lat'])
            print(']')
    print(']}}')

print('{"type": "FeatureCollection", "features": [')

for item in root.iter('node'):
    nodes[item.attrib.get('id')] = item

types = {}
first = True
for item in root.iter('way'):
    highway = ''
    name = ''
    for subitem in item:
        if subitem.attrib.get('k') == 'name':
            name = subitem.attrib.get('v')
        if subitem.attrib.get('k') == 'highway':
            highway = subitem.attrib.get('v')
    if highway not in ['footway', 'service', 'steps', 'path', 'platform',
                       'pedestrian', '']:
        if first:
            first = False
        else:
            print(',')
        dumpWay(item, highway, name)
    types[highway] = name

print(']}')

print(json.dumps(types))
