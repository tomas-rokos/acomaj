import logging
import os
import shutil


logging.basicConfig(level=logging.INFO)

project = 'acomaj-b06bc'

# os.system('gcloud auth login')
logging.info('setting project to %s', project)
os.system('gcloud config set project ' + project)


def create_cloud_function(function_name, trigger_type, resource_loc, timeout=60):
    logging.info('Deploying %s', function_name)
    # trigger_type - create, update, delete, write
    trigger = 'providers/cloud.firestore/eventTypes/document.' + trigger_type
    resource = '/databases/(default)/documents' + resource_loc

    os.system('gcloud functions deploy ' + function_name + ' --runtime python37'
              + ' --source ./functions/' + function_name
              + ' --trigger-event ' + trigger
              + ' --trigger-resource "projects/' + project + resource + '"'
              + ' --timeout=' + str(timeout))


create_cloud_function('ingester', 'create', '/places/{place}/ingest/{date}')
create_cloud_function('headliner', 'create', '/menu/{menu}')
